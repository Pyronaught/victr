$(document).ready(function () {
    $('#projects').DataTable({
        "autoWidth": false,
        "order": [[6, "desc"]],
        "dom": "<'right'f>t<'row'<'col-sm-4'i><'col-sm-4'p><'col-sm-4 text-right'l>>",
        "columnDefs": [
            {"orderData": [7], "targets": [4]},
            {"orderData": [8], "targets": [5]},
            {
                "targets": [7, 8],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [0],
                "width": "10%"
            },
            {
                "targets": [1],
                "width": "12%"
            },
            {
                "targets": [2],
                "width": "26%"
            },
            {
                "targets": [3],
                "width": "20%"
            },
            {
                "targets": [4, 5],
                "width": "10%"
            },
            {
                "targets": [6],
                "width": "10%"
            }
        ]
    });
});