<?php

namespace VictrBundle\Service;

use Resty\Resty;
use Symfony\Component\DependencyInjection\Container;

/**
 * Created by IntelliJ IDEA.
 * User: ctompkins
 * Date: 8/3/2017
 * Time: 1:02 PM
 */
class GitHubData
{
    /** @var Container */
    private $container;
    
    public function __construct($container)
    {
        $this->container = $container;
    }
    
    public function getTopGithubProjects()
    {
        $queryParams = [
            'q'     => 'language:php',
            'sort'  => 'stars',
            'order' => 'desc'
        ];
        $resty       = new Resty(['json_to_array' => true]);
        $response    = $resty->get("https://api.github.com/search/repositories", $queryParams);
        if ($response['body'] && $response['body']['items']) {
            return $response['body']['items'];
        }
    }
    
    public function refreshProjects()
    {
        $em          = $this->container->get('doctrine')->getManager();
        $projectRepo = $this->container->get('doctrine')->getRepository('VictrBundle:Project');
        $projects    = $this->getTopGithubProjects();
        foreach ($projects as $project) {
            $projectEnt = !is_null($projectRepo->find($project['id'])) ? $projectRepo->find($project['id']) : new Project();
            $projectEnt
                ->setId($project['id'])
                ->setName($project['name'])
                ->setUrl($project['html_url'])
                ->setCreatedDate(new \DateTime($project['created_at']))
                ->setLastPushDate(new \DateTime($project['pushed_at']))
                ->setDescription($project['description'])
                ->setStars($project['stargazers_count']);
            
            $em->merge($projectEnt);
        }
        $em->flush();
    }
}