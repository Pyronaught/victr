<?php

namespace VictrBundle\Controller;

use Resty\Resty;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use VictrBundle\Entity\Owner;
use VictrBundle\Entity\Project;
use VictrBundle\Service\GitHubData;

class ProjectController extends Controller
{
    /**
     * @Route("/")
     * @Template()
     */
    public function indexAction()
    {
        $gitHubService = $this->get(GitHubData::class);
        $gitHubService->refreshProjects();
        $projectRepo = $this->getDoctrine()->getRepository('VictrBundle:Project');
        $projects    = $projectRepo->findAll();
        
        return ['projects' => $projects];
    }
    
    
}
