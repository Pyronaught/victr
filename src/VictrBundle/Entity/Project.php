<?php

namespace VictrBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Project
 * repository ID, name, URL, created date, last push date, description, and
    number of stars.
 * @ORM\Table(name="project")
 * @ORM\Entity(repositoryClass="VictrBundle\Repository\ProjectRepository")
 */
class Project
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     */
    private $id;
    
    /**
     * @var string $name
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;
    
    /**
     * @var string $url
     * @ORM\Column(name="url", type="string", length=255)
     */
    private $url;
    
    /**
     * @var \DateTime $createdDate
     * @ORM\Column(name="created_date", type="datetime")
     */
    private $createdDate;
    
    /**
     * @var \DateTime $lastPushDate
     * @ORM\Column(name="last_push_date", type="datetime")
     */
    private $lastPushDate;
    
    /**
     * @var string $description
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;
    
    /**
     * @var int $stars
     * @ORM\Column(name="stars", type="integer")
     */
    private $stars;
    
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
    
    /**
     * @param int $id
     * @return Project
     */
    public function setId(int $id): Project
    {
        $this->id = $id;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
    
    /**
     * @param string $name
     * @return Project
     */
    public function setName(string $name): Project
    {
        $this->name = $name;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }
    
    /**
     * @param string $url
     * @return Project
     */
    public function setUrl(string $url): Project
    {
        $this->url = $url;
        
        return $this;
    }
    
    /**
     * @return \DateTime
     */
    public function getCreatedDate(): \DateTime
    {
        return $this->createdDate;
    }
    
    /**
     * @param \DateTime $createdDate
     * @return Project
     */
    public function setCreatedDate(\DateTime $createdDate): Project
    {
        $this->createdDate = $createdDate;
        
        return $this;
    }
    
    /**
     * @return \DateTime
     */
    public function getLastPushDate(): \DateTime
    {
        return $this->lastPushDate;
    }
    
    /**
     * @param \DateTime $lastPushDate
     * @return Project
     */
    public function setLastPushDate(\DateTime $lastPushDate): Project
    {
        $this->lastPushDate = $lastPushDate;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    
    /**
     * @param string $description
     * @return Project
     */
    public function setDescription($description): Project
    {
        $this->description = $description;
        
        return $this;
    }
    
    /**
     * @return int
     */
    public function getStars(): int
    {
        return $this->stars;
    }
    
    /**
     * @param int $stars
     * @return Project
     */
    public function setStars(int $stars): Project
    {
        $this->stars = $stars;
        
        return $this;
    }
   
    
}

