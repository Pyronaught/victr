GitHub Top Projects
======
Author: Clint Tompkins - [clintdt41@gmail.com](mailto:clintdt41@gmail.com)

Purpose: VICTR Assessment

Frameworks and Tools used:

*  PHP 7.0
* [Symfony 3.3](https://symfony.com/)
* [Datatables 1.10.15](https://datatables.net/)
* [jquery 2.2.4](https://jquery.com/)
* [Bootstrap 3.3.7](http://getbootstrap.com/)
* [Resty](https://github.com/fictivekin/Resty.php)

---

This project uses Symfony and Doctrine to call the GitHub API to retrieve the top PHP projects by number of stars.

The architecture is simply a single controller action which calls a service to retrieve and store the projects. Then it outputs that data and formats it using jquery and Datatables.
 
 
### Requirements

* PHP 7.0 or greater
* MYSQL
* Composer

### Installation

1. Clone project
2. Copy app/config/parameters.yml.dist to parameters.yml and fill in your DB info 
3. Run composer install in project directory
4. Fix any remaining permissions isses in the var/ directory [link](https://symfony.com/doc/current/setup/file_permissions.html)
5. On command line, run the following in the project root  
    ```
    php bin/console doctrine:schema:update --force
    ```
    
    ```
    php bin/console cache:clear --env=prod
    ```  
    
6. Do whatever you need to do with apache or nginx to get it running in browser
7. Enjoy!


###### If I missed anything just let me know and I'll see if I can assist.





